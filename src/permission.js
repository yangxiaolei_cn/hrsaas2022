// 权限拦截在路由跳转 导航守卫
import router from '@/router'
import store from '@/store' // 相当于this.
import NProgress from 'nprogress' // 1.引入一份进度条插件
import 'nprogress/nprogress.css' // 2.引入进度条样式
// 3.开启start和关闭done

// 路由拦截
// 前置守卫
// next是前置守卫必须必须必须执行的钩子，next如果不执行，页面就死了
// next() 放过
// next(fale) 跳转终止
// next(地址) 跳转到某个地址
const whiteList = ['/login', '/404'] // 定义白名单
// to 去哪 from从哪里 next跳转
router.beforeEach(async(to, from, next) => {
  NProgress.start() // 开启进度条
  // 首先判断有无token
  if (store.getters.token) {
    // 是否是登录页
    if (to.path === '/login') {
      // 跳转主页
      next('/')
    } else {
      // 当前已经有token,没有UserInfo
      if (!store.getters.userId) {
        // 如果没有id这个值 才会调用 vuex的获取资料的action
        const { roles } = await store.dispatch('user/getUserInfo')
        // 为什么要写await 因为我们想获取完用户资料再去放行
        // 筛选用户的可用路由
        const routes = await store.dispatch('permission/filterRoutes', roles.menus)
        // routes就是筛选得到的动态路由
        // 动态路由 添加到 路由表中 默认的路由表 只有静态路由 没有动态路由
        // addRoutes 必须用 next(地址) 不能用next() 这是缺陷
        router.addRoutes([...routes, { path: '*', redirect: '/404', hidden: true }])
        // 添加完动态路由后
        next(to.path) // 相当于跳到对应的地址  相当于多做一次跳转 为什么要多做一次跳转
        // 进门了，但是进门之后我要去的地方的路还没有铺好，直接走，掉坑里，
        // 多做一次跳转，再从门外往里进一次，跳转之前 把路铺好，再次进来的时候，路就铺好了
      } else {
        next()
      }
    }
  } else {
    // console.log(1)
    if (whiteList.indexOf(to.path) > -1) {
      // 表示在白名单
      next()
    } else {
      next('/login')
    }
  }
  NProgress.done() // 手动切换地址时，强制关闭进度条
})
// 后置守卫
router.afterEach(() => {
  NProgress.done() // 关闭进度条
})
