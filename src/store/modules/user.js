import { login, getUserInfo, getUserDetailById } from '@/api/user'
import { getToken, setToken, removeToken, setTimeStamp } from '@/utils/auth'
import { resetRouter } from '@/router/index'

// const getDefaultState = () => {
//   return {
//     token: getToken(),
//     name: '',
//     avatar: ''
//   }
// }

const state = {
  token: getToken(), // 设置token为共享状态，初始化vuex时，先从缓存中读取
  userInfo: {} // 定义一个空的对象 不是null 因为后边我要开发userInfo的属性给别人用
}

const mutations = {
  // 设置token
  setToken(state, token) {
    state.token = token // 设置token  只是修改state的数据  123 =》 1234
    // vuex变化 => 缓存数据
    setToken(token) // vuex和 缓存数据的同步
  },
  // 删除缓存
  removeToken(state) {
    state.token = null // 删除vuex的token
    removeToken() // 先清除 vuex  再清除缓存 vuex和 缓存数据的同步
  },
  setUserInfo(state, userInfo) {
    // state.userInfo = result // 这是响应式
    state.userInfo = { ...userInfo } // 用 浅拷贝的方式去赋值对象
  },
  // 删除用户信息
  reomveUserInfo(state) {
    state.userInfo = {}
  }
}

const actions = {
  async login(context, data) {
    // 调用api的接口
    const result = await login(data)
    // result -> promise对象 -> 只考虑成功，失败的情况交给请求拦截器
    context.commit('setToken', result)
    setTimeStamp()
  },
  // async getUserInfo(context) {
  //   const result = await getUserInfo()
  //   context.commit('setUserInfo', result)
  //   // 为了调用函数时拿到值
  //   return result
  // },
  // 获取用户资料action
  async getUserInfo(context) {
    const result = await getUserInfo() // result就是用户的基本资料
    const baseInfo = await getUserDetailById(result.userId) // 为了获取头像
    const baseResult = { ...result, ...baseInfo } // 将两个接口结果合并
    // 此时已经获取到了用户的基本资料 迫不得已 为了头像再次调用一个接口
    context.commit('setUserInfo', baseResult) // 提交mutations
    // 加一个点睛之笔  这里这一步，暂时用不到，但是请注意，这给我们后边会留下伏笔
    return baseResult
  },
  // 登出
  logout(context) {
    // 删除token
    context.commit('removeToken')
    // 删除用户资料
    context.commit('reomveUserInfo')
    // 重置路由
    resetRouter() // 去设置权限模块下路由为初始状态
    // 不加命名空间的情况下，所有的mutations 和action 都是挂在全局上的 所以可以直接调用。
    // 家了命名空间的子模块，怎么调用另一个加了命名空间的子模块的mutations
    // 要清空permission模块下的state数据
    // vuex中 user子模块  permission子模块
    // 子模块调用子模块的action  默认情况下 子模块的context是子模块的
    // 父模块 调用 子模块的action
    context.commit('permission/setRoutes', [], { root: true })
    // 子模块调用子模块的action 可以 将 commit的第三个参数 设置成  { root: true }
    // 就表示当前的context不是子模块了 而是父模块
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

