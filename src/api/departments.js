import request from '@/utils/request'

// 组织架构
export function getDepartments() {
  return request({
    url: '/company/department'
  })
}

// 删除接口
export function dalDepartments(id) {
  return request({
    url: `/company/department/${id}`,
    method: 'delete' // 接口满足接口规范
    // 删除delete 新增post 修改put 获取get
  })
}

// 增加接口
export function addDepartments(data) {
  return request({
    url: '/company/department',
    method: 'post', // 接口满足接口规范
    // 删除delete 新增post 修改put 获取get
    data
  })
}

// 获取点击部门的详情
export function getDepartDetail(id) {
  return request({
    url: `/company/department/${id}`
  })
}

// 编辑部门
export function updateDepartments(data) {
  return request({
    url: `/company/department/${data.id}`,
    method: 'put',
    data
  })
}
