// 多语言实例化

import Vue from 'vue'
import VueI18n from 'vue-i18n'
import Cookie from 'js-cookie' // 引入cookie包
import elementEN from 'element-ui/lib/locale/lang/en' // 引入饿了么的英文包
import elementZH from 'element-ui/lib/locale/lang/zh-CN' // 引入饿了么的中文包
import elementJA from 'element-ui/lib/locale/lang/ja' // 引入饿了么的中文包
Vue.use(VueI18n) // 全局注册国际化包
export default new VueI18n({
  // 设置国际化语言的地方
  locale: Cookie.get('language') || 'zh', // 从cookie中获取语言类型 获取不到就是中文
  // 设置国际化语言包的地方
  messages: {
    en: {
      ...elementEN // 将饿了么的英文语言包引入 elemengtUi 语言包 与 自定义语言包
    // ...customLanguage 自定义语言包
    },
    zh: {
      ...elementZH // 将饿了么的中文语言包引入
    },
    ja: {
      ...elementJA
    }
  }
})
