import store from '@/store'
// 负责2

export const imagerror = {
  // 5个周期:
  // 指令对象
  inserted(dom, options) {
    // options是指令的变量的解释 其中有一个属性 value
    // dom 表示当前指令作用的dom对象
    // dom 认为此时就是图片
    dom.src = dom.src || options.value
    // 当图片有地址,但地址没有加载成功的时候，会报错，会触发图片 onerror事件
    dom.onerror = function() {
      // 当图片出现异常时，指令会配置默认图片的路径为这个图片的路径
      dom.src = options.value
    }
  },
  // 该函数同inserted一样也是一个钩子函数
  componentUpdated(dom, options) {
    // 该钩子函数会在当前指令作用的组件，
    dom.src = dom.src || options.value
  }
}

// 全局指令
export const auth = {
  inserted(dom, options) {
    const { userInfo } = store.state.user
    if (!userInfo?.roles?.points.includes(options.value)) {
      dom.style.display = 'none'
    }
  }
}
