import store from '@/store'
import axios from 'axios'
import router from '@/router'
import { Message } from 'element-ui'
import { IsCheckTimeOut } from '@/utils/auth'

// import store from '@/store'
// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API,
  // process.env.VUE_APP_BASE_API = 开发环境的'/api'
  timeout: 10000 // request timeout
})

// 请求拦截器
service.interceptors.request.use(config => {
  // 在这个位置需要统一的去注入token
  // 处了要检测有无token还要检测你的token是否有效
  if (store.getters.token) {
    // 判断时效性
    if (IsCheckTimeOut()) {
      // 如果它为true表示 过期了
      // token没用了 因为超时了
      store.dispatch('user/logout') // 登出操作
      // 跳转到登录页
      router.push('/login')
      return Promise.reject(new Error('token超时了,请重新登录'))
    }
    // 如果token存在 注入token
    config.headers['Authorization'] = `Bearer ${store.getters.token}`
  }
  return config // 必须返回配置
},
error => {
  return Promise.reject(error)
})

// 响应拦截器
service.interceptors.response.use((response) => {
  // axios默认加了一层data
  const { success, message, data } = response.data
  // 要根据success的成功与否决定下面的操作
  if (success) {
    return data
  } else {
    // 业务已经错误了 还能进then ? 不能 ！ 应该进catch
    Message.error(message)
    return Promise.reject(new Error(message))
  }
}, error => {
  // error 里有一个response 对象
  if (error.response && error.response.data && error.response.data.code === 10002) {
    // 当等于10002时 就告诉你token超时了
    store.dispatch('user/logout') // 登出操作
    // 跳转到登录页
    router.push('/login')
  } else {
    Message.error(error.message) // 提示错误
  }
  return Promise.reject(error) // 返回错误
})

// 是否超时
// function IsCheckTimeOut() {
//   var currenTime = Date.now() // 当前时间戳
//   var timeStamp = getTimeStamp() // 缓存时间戳
//   return (currenTime - timeStamp) / 1000 > TimeOut
// }
export default service
