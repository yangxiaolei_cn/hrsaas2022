import store from '@/store'

// 做混入对象
export default {
  // 混入对象是组件的选择对象
  methods: {
    // 帮助页面按钮检测是否有执行权限
    // 去用户的信息里去找，
    checkPermission(key) {
      // key 就是按钮的权限
      //  store.state.user.userInfo.roles,points 是个 数组
      const { userInfo } = store.state.user
      // return userInfo?.roles?.item
      if (userInfo.roles.points && userInfo.roles.points.length) {
        return userInfo.roles.points.some(item => item === key)
      }
      return false
    }
  }
}
